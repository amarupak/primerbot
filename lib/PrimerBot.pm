use MooseX::Declare;

use strict;
use warnings;

class PrimerBot with Logger {

#####////}}}}}

#### EXTERNAL MODULES
use File::Basename;
use Excel::Writer::XLSX;
use FindBin qw($Bin);

#### INTERNAL MODULES
use Conf::Yaml;

# Integers
has 'log'		=>  ( isa => 'Int', is => 'rw', default => 2 );
has 'printlog'	=>  ( isa => 'Int', is => 'rw', default => 5 );
has 'sleep'			=> 	( isa => 'Str|Undef', is => 'rw', default	=>	10 );

# Strings

# Objects
has 'conf'			=> ( isa => 'Conf::Yaml', is => 'rw', lazy => 1, builder => "setConf" );

method run ($inputfile) {
	$self->logDebug("inputfile", $inputfile);
	
	#### CHECK EXECUTABLES
	$self->checkExecutables();
	
	#### CHECK REFERENCE
	$self->checkReference();
	
	#### PROCESS FILE
	$self->processInputFile($inputfile);
}

method processInputFile ($inputfile) {
	#### GET SEQUENCES FROM BED FILE
	#my $bedinfile = $inputfile;
	#my $bedoutinputfile = $results_dir.$displaydate.'_fastafrombed.fasta';
	#my $genomefapath;

	#if ($genome eq "HG19"){
	#    $genomefapath = $genomefapathhg19;
	#}elsif($genome eq "MM10") {
	#    $genomefapath = $genomefapathmm10;
	#}

	my $installdir = $self->getInstallDir("bedtools2");
	my $bedtools = "$installdir/bin/bedtools";
	$self->logDebug("bedtools", $bedtools);
	
	
    $installdir = $self->getInstallDir("primer3");
	my $primer3 = "$installdir/src/primer3_core";
	$self->logDebug("primer", $primer3);
	
	
	$installdir = $self->getInstallDir("ucsc.hg19");
	my $referencefile = "$installdir/ucsc.hg19.fasta";
	$self->logDebug("referencefile", $referencefile);
	my $fastafile = "$inputfile.fasta";
	
	
	system("$bedtools", "getfasta", "-fi", "$referencefile", "-bed", $inputfile, "-name" ,"-fo", "$fastafile");

	
	if ( $? == 1 )
	{
		print "command failed: $!\n";
	}
	else
	{
		printf "command exited with value %d", $? >> 8;
	}
	
	#create local excelworksheet to write output to.
	my $excelresults= $inputfile.'_ChIP_PrimerResults.xlsx';
	my $workbook = Excel::Writer::XLSX->new($excelresults);
	    die "Problems creating new Excel file: $!" unless defined $workbook;
	my $worksheet = $workbook->add_worksheet();
	my @header= ("Index", "Location","Genome","Primer_Type","Orientation", "Start", "Len", "TM", "GC", "Any Compl", "3' Compl", "Primer Name (chr:start_pos_F/R)", "Sequence", "Prod Size", "Peak Size", "Pair Any Comp", "Pair 3' Comp", "UCSC Link" );
	$worksheet->write_row( 0 , 0 , \@header);
	
	my $start_time = time();
	my @passedsets;
	my $rowcount = 1;
	my $index = 0;
	my ($count, $targets, $input, @failed);
	
	#get sequences from file we just wrote.
	
	my %seqs = %{ $self->read_fasta_as_hash($fastafile) };
	print Dumper %seqs;
	my $sequence;
	foreach my $id ( keys %seqs ) {
	    $count++;
	    #print "<p>".$count,"</p>";
	    print "<p>".$id.",    ";
	    #print uc($seqs{$id}), "\n";
	    my $peakwidth =length($seqs{$id});
	    print "Length=$peakwidth";
	    my $targets = int(($peakwidth)/2).",1";
		my $number_returned = 5;
	    #print $targets, "\n\n";

	    my $input = $self->set_primer3_parameters((uc$seqs{$id}),$id, $targets,$number_returned);
	    #print Dumper $input,"\n";

	    my @results = $self->run_primer3($input);
	    #print Dumper @results;

	    $self->export_primers($id,@results);
	    print "<hr>";
	}

}

method getInstallDir ($packagename) {
	$self->logDebug("packagename", $packagename);

	my $packages = $self->conf()->getKey("packages:$packagename", undef);
	$self->logDebug("packages", $packages);
	my $version	=	undef;
	foreach my $key ( %$packages ) {
		$version	=	$key;
		last;
	}

	my $installdir	=	$packages->{$version}->{INSTALLDIR};
	$self->logDebug("installdir", $installdir);
	
	return $installdir;
}


method checkExecutables {
##put all paths here
#$ENV{'BEDTOOLS'} = '/a/apps/bedtools2/2.19.1/bin//';
#my $PROGRAM_DIR = $ENV{'BEDTOOLS'};
#$ENV{PATH} = "$PROGRAM_DIR:$ENV{PATH}" if $PROGRAM_DIR;
#my $primer3path ='/a/apps/primer3/2.3.6/src/primer3_core';
#my $PRIMER_THERMODYNAMIC_PARAMETERS_PATH= '/a/apps/primer3/2.3.6/src/primer3_config/';
#my $bedtoolspath ='/a/apps/bedtools2/2.19.1/bin';
#my $basedirectory="/a/apps/PrimerBot/latest/htdocs/";

	my $installdir = $self->getInstallDir("primer3");
	$self->logDebug("installdir", $installdir);
	
	
	
##Set Primer3 path  and bedtools path
#my $PRIMER3_EXE = $primer3path;
#unless (-e $PRIMER3_EXE) {
#    print '<div class="alert alert-warning">';
#  	print "<h4> Cannot find the Primer3 executable! </div>";
#	exit() ;
#}
#
	$installdir = $self->getInstallDir("bedtools2");
	$self->logDebug("installdir", $installdir);
#my $BEDTOOLS_EXE = $bedtoolspath;
#unless (-x $BEDTOOLS_EXE) {
#  	print '<div class="alert alert-warning">';
#  	print "<h4> Cannot find the bedtools executable! </div>";
#	exit() ;
#}
#
	
}

method checkReference {
	$self->logDebug("");
	my $installdir = $self->getInstallDir("ucsc.hg19");
	

	#my $genomefapathhg19= '/a/apps/PrimerBot/latest/htdocs/files/ucsc.hg19.fasta';
#my $genomefapathmm10= '/home/primerbot/resources/mm10_genome.fa';

#print $upload_dir;
#print $filename;


	
}



#subroutines
method export_primers {
    my($id,@results)=@_;
    my $primer3output = new Primer3Output();
    $primer3output->set_results(\@results);
    #print Dumper $primer3output;
    #print Dumper @results;
    my $primer_list = $primer3output->get_primer_list();
   
    if (!defined ($primer_list)) {
        print "Primer3 -> Unable to design primers".$id." \n";
        push (my @failed, $id);
    }
    
    #print Dumper @$primer_list;
    
    foreach my $primer_pair (@$primer_list) {
	if (!defined ($primer_pair->left_primer())) {
	    print "Primer3 -> Unable to design primers for the sequence".$id." \n";
	    
	}
        
	my $index++;
	push (my @passedsets, $id);
	
	#name for primers
	 my @primername = split(/\:|\-/, $id);
	$primername[0]  =~ s{^\s+|\s+$}{}g; #removes all whitespace and lines
    $primername[0]  =~ s/\W//g; #removes all but alphanumeric
	
	#print forward primer
	my $forward_primer_obj = $primer_pair->left_primer();
	my @row_forward = (
	    $forward_primer_obj->direction(),
	    $forward_primer_obj->start(),
	    $forward_primer_obj->length(),
	    $forward_primer_obj->tm(),
	    $forward_primer_obj->gc(),
	    $forward_primer_obj->any_complementarity(),
	    $forward_primer_obj->end_complementarity(),
	    my $forname = $primername[0].$primername[1]."_".$forward_primer_obj->start()."_F",
	    uc($forward_primer_obj->sequence()),
	    $primer_pair->product_size(),
	    $primer_pair->seq_size(),
	    $primer_pair->pair_any_complementarity(),
	    $primer_pair->pair_end_complementarity()
	);
		    
	my $reverse_primer_obj = $primer_pair->right_primer();
	my @row_reverse = (
	    $reverse_primer_obj->direction(),
	    $reverse_primer_obj->start(),
	    $reverse_primer_obj->length(),
	    $reverse_primer_obj->tm(),
	    $reverse_primer_obj->gc(),
	    $reverse_primer_obj->any_complementarity(),
	    $reverse_primer_obj->end_complementarity(),
	    my $revname = $primername[0].$primername[1]."_".$reverse_primer_obj->start()."_R",
	    uc($reverse_primer_obj->sequence())
	);
    
   	my $mmChIP_URL = '"http://genome.ucsc.edu/cgi-bin/hgPcr?hgsid=372658971_rrGfWWTxsDRCMUHlxnaRzpQ8aWFh&org==Mouse&db=mm10&wp_target=genome&wp_f='.$row_forward[8]."+&wp_r=".$row_reverse[8].'+&Submit=submit&wp_size=4000&wp_perfect=15&wp_good=15&boolshad.wp_flipReverse=0"';
	my $hgChIP_URL = '"http://genome.ucsc.edu/cgi-bin/hgPcr?hgsid=320998975&org=Human&db=hg19&wp_target=genome&wp_f='.$row_forward[8]."+&wp_r=".$row_reverse[8].'+&Submit=submit&wp_size=4000&wp_perfect=15&wp_good=15&boolshad.wp_flipReverse=0"';
	my $mylink;
	
	if (my $genome eq "HG19") {
	    $mylink ='=HYPERLINK('.$hgChIP_URL.',"UCSC")';
	}
       if (my $genome eq "MM10") {
	    $mylink ='=HYPERLINK('.$mmChIP_URL.',"UCSC")';
	}
        
	my $worksheet;
	my $rowcount;
	
	$worksheet->write( $rowcount, 0 , $index);
	$worksheet->write( $rowcount, 1 , $id);
        $worksheet->write( $rowcount, 2 , "$genome");
	$worksheet->write( $rowcount, 3, "ChIP");
	$worksheet->write( $rowcount, 4 , \@row_forward);
	$worksheet->write( $rowcount, 17, $mylink);
    
	$rowcount++;
	
	$worksheet->write( $rowcount, 0 , $index);
	$worksheet->write( $rowcount, 1 , $id);
        $worksheet->write( $rowcount, 2 , "$genome");
	$worksheet->write( $rowcount, 3, "ChIP");
	$worksheet->write( $rowcount, 4 , \@row_reverse);
	
	 $rowcount++;
    }
}

method set_primer3_parameters {
    my ($seq,$seqid, $targets, $number_returned) = @_;
    my $PRIMER_TASK ='generic';
    my $PRIMER_PICK_LEFT_PRIMER=1;
    my $PRIMER_PICK_RIGHT_PRIMER=1;
    my $PRIMER_OPT_SIZE = 20;
    my $PRIMER_MIN_SIZE = 18;
    my $PRIMER_MAX_SIZE = 27;
    my $PRIMER_MAX_NS_ACCEPTED = 1;
    my $PRIMER_PRODUCT_SIZE_RANGE = '60-100 60-150';
    my $P3_FILE_FLAG=1;
    my $PRIMER_EXPLAIN_FLAG = 1;
    my $PRIMER_LIBERAL_BASE = 1;

    
    ####generate file for input into primer3.
    my @input = ();
    push(@input, "SEQUENCE_ID=$seqid\n");
    push(@input, "SEQUENCE_TEMPLATE=$seq\n");
    push(@input, "PRIMER_TASK=$PRIMER_TASK\n");
    push(@input, "PRIMER_PICK_LEFT_PRIMER=$PRIMER_PICK_LEFT_PRIMER\n");
    push(@input, "PRIMER_PICK_RIGHT_PRIMER=$PRIMER_PICK_RIGHT_PRIMER\n");
    push(@input, "PRIMER_OPT_SIZE=$PRIMER_OPT_SIZE\n");
    push(@input, "PRIMER_MIN_SIZE=$PRIMER_MIN_SIZE\n");
    push(@input, "PRIMER_MAX_SIZE=$PRIMER_MAX_SIZE\n");
    push(@input, "SEQUENCE_TARGET=$targets\n");
    push(@input, "PRIMER_NUM_RETURN=$number_returned\n");
    push(@input, "PRIMER_MIN_THREE_PRIME_DISTANCE=5\n"); #can also be 0 or -1.
    push(@input, "PRIMER_PRODUCT_SIZE_RANGE=$PRIMER_PRODUCT_SIZE_RANGE\n");
    push(@input, "PRIMER_EXPLAIN_FLAG=$PRIMER_EXPLAIN_FLAG\n");
    push(@input, "PRIMER_LIBERAL_BASE=$PRIMER_LIBERAL_BASE\n");
    push(@input, "PRIMER_THERMODYNAMIC_PARAMETERS_PATH=$PRIMER_THERMODYNAMIC_PARAMETERS_PATH\n"); 
    push(@input, "=\n"); 
    return \@input;
}

## This function is to run primer3 core program and return the primer design results.
method run_primer3{
    my ($input) = @_;
    my $cmd = "$PRIMER3_EXE -format_output -strict_tags";

    my $primer3_pid;
    my %results = ();
    
    my ($childin, $childout, $childerr) = (FileHandle->new, FileHandle->new, FileHandle->new);
    $primer3_pid = open3($childin, $childout, $childerr, $cmd);
    if (!$primer3_pid) {
        print "Cannot execute $cmd:<br>$!";
        exit;
    }

    print $childin @$input;
    $childin->close;
  
    my @results = $childout->getlines;
  
    waitpid $primer3_pid, 0;
    return @results;

}
#
method read_fasta_as_hash {
    my $current_id = '';
    my %seqs;
    open MULTIFASTA, "<@_" or die $!;
    while ( my $line = <MULTIFASTA> ) {
        chomp $line;
        if ( $line =~ /^(>.*)$/ ) {
            $current_id  = $1;
        } elsif ( $line !~ /^\s*$/ ) { # skip blank lines
            $seqs{$current_id} .= $line
        }
    }
    close MULTIFASTA or die $!;

    return \%seqs;
}


	
}

